# GASCHE-SZCZECH

## Name
FRIDAY

## Installation
Launch
```
./mvnw clean package
```

The jar will appear in

```
friday-back/target/quarkus-app/quarkus-run.jar
```

Launch

```
java -jar quarkus-run.jar
```

To launch the app and go to

```
http://localhost:8080/
```

to use the calendar.


## Authors and acknowledgment
Mateusz SZCZECH

Luca GASCHE

